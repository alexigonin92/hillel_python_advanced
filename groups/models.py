import uuid
from random import random

from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from teachers.models import Teacher


class Classroom(models.Model):
    name = models.CharField(max_length=64)
    floor = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(2), MaxValueValidator(5)]
    )

    def __str__(self):
        return f'{self.name}, floor: {self.floor}'


class Group(models.Model):
    name = models.CharField(max_length=64, null=False)
    uu_id = models.UUIDField(default=uuid.uuid4)
    teacher = models.ForeignKey(to=Teacher, null=True, on_delete=models.SET_NULL, related_name='groups')
    curator = models.OneToOneField(to=Teacher, null=True, on_delete=models.SET_NULL, related_name='group_curator')

    leader = models.OneToOneField(to='students.Student',
                                  null=True,
                                  on_delete=models.SET_NULL,
                                  related_name='group_leader')

    classroom = models.ManyToManyField(to=Classroom, related_name='groups')

    def clean(self):
        if self != self.leader.group:
            raise ValidationError('В группе нет такого студента')

    def save(self, *args, **kwargs):
        self.clean()
        super().save(self, *args, **kwargs)

    def __str__(self):
        return f'{self.name}'

    @staticmethod
    def create_group(group_count):
        teachers = list(Teacher.objects.all())
        groups = ['Python', 'Java', 'JavaScript', 'c++']

        for _ in range(int(group_count)):
            name = random.choice(groups)
            uu_id = uuid.uuid4()
            teacher = random.choice(teachers)

            Group.objects.create(name=name, uu_id=uu_id, teacher=teacher)
