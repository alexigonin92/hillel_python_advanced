from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from groups.forms import GroupCreateForm
from groups.models import Group


class GroupListView(LoginRequiredMixin, ListView):
    model = Group
    template_name = 'group-get.html'
    context_object_name = 'groups'
    paginate_by = 6

    def get_queryset(self):
        """
        Функция осуществляет фильтрацию

        super().get_queryset() - наследование от базового класса. Аналог записи Group.objects.all()

        self.request - в классе GroupListView есть доступ к request

        qr - список обьектов класса Group.
        """
        qr = super().get_queryset()
        request = self.request

        name = request.GET.get('name')
        code_language = request.GET.get('code_language')
        students_count = request.GET.get('students_count')

        if name:
            qr = qr.filter(name=name)
        if code_language:
            qr = qr.filter(code_language=code_language)
        if students_count:
            qr = qr.filter(students_count=students_count)

        return qr


class GroupCreateView(LoginRequiredMixin, CreateView):
    """
    Класс реализует функционал создания группы

     UpdateView - встроенный класс Джанго, стандарная реализация создания.
        переменные задаются как написано в документации.
    """
    model = Group
    form_class = GroupCreateForm
    template_name = 'group-create.html'
    success_url = reverse_lazy('groups:list')


class GroupUpdateView(UpdateView):
    """
         Класс реализует функционал редактирования группы

         UpdateView - встроенный класс Джанго, стандарная реализация редактирования.
            переменные задаются как написано в документации.

         # get_object - возвращает группу по uu_id.

         get_context_data - расширяет словарь context в функции render()

        """
    model = Group
    form_class = GroupCreateForm
    success_url = reverse_lazy('groups:list')
    template_name = 'group-edit.html'
    context_object_name = 'group'

    def get_object(self):
        """
        self - это обьект класса GroupUpdateView

        self.kwargs.get('uuid') - kwargs это словарь, get('uuid') функция возвращает значение ключа 'uuid'

        self.get_queryset() - возвращает объект модели. В данном случае по конкретному айдишнику.
        """

        uuid = self.kwargs.get('uuid')
        return self.get_queryset().get(uu_id=uuid)

    def get_context_data(self, **kwargs):
        """
        ????  super().get_context_data(**kwargs) -- Не совсем понял как это работает ????
        """
        context = super().get_context_data(**kwargs)
        context['students'] = self.get_object().students.all()
        return context


class GroupDeleteView(LoginRequiredMixin, DeleteView):
    model = Group
    success_url = reverse_lazy('groups:list')

    def get_object(self):
        """
        self - это обьект класса GroupDeleteView

        self.kwargs.get('uuid') - kwargs это словарь, get('uuid') функция возвращает значение ключа 'uuid'

        self.get_queryset() - возвращает объект модели. В данном случае по конкретному айдишнику.
        """

        uuid = self.kwargs.get('uuid')
        return self.get_queryset().get(uu_id=uuid)

    def delete(self, request, *args, **kwargs):
        """
        Стандартный метод базового класса

        self.get_object() - Принимает один обьект из метода get_object, который я переопределил

        self.get_success_url() - подхватывает атрибут класса
        """
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.delete()
        return HttpResponseRedirect(success_url)
