from django.urls import path

from groups.views import GroupCreateView, GroupDeleteView, GroupListView, GroupUpdateView
app_name = 'groups'

urlpatterns = [
    path('', GroupListView.as_view(), name='list'),
    path('create', GroupCreateView.as_view(), name='create'),
    path('edit/<uuid:uuid>', GroupUpdateView.as_view(), name='edit'),
    path('delete/<uuid:uuid>', GroupDeleteView.as_view(), name='delete'),

]
