from django.contrib import admin # noqa

# Register your models here.
from groups.models import Classroom, Group

from students.models import Student


class StudentTable(admin.TabularInline):
    model = Student
    fields = ['first_name', 'last_name']
    readonly_fields = fields
    show_change_link = True


class GroupAdmin(admin.ModelAdmin):
    list_display = ['name', 'teacher', 'leader']
    inlines = [StudentTable]
    list_select_related = ['teacher', 'leader']


admin.site.register(Classroom)
admin.site.register(Group, GroupAdmin)
