# Generated by Django 3.1 on 2020-09-13 06:58

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('groups', '0007_auto_20200911_1551'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='uu_id',
            field=models.UUIDField(default=uuid.UUID('ad776c76-64ff-4d6a-ab8f-11ff34dc3351')),
        ),
    ]
