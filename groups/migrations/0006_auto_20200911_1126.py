# Generated by Django 3.1 on 2020-09-11 11:26

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('groups', '0005_group_uu_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='uu_id',
            field=models.UUIDField(default=uuid.UUID('73639649-f877-429a-b28b-f5e2b78dde0d')),
        ),
    ]
