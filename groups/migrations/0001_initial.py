import datetime

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True,
                                        serialize=False, verbose_name='ID')),
                ('group_name', models.CharField(max_length=64)),
                ('language', models.CharField(max_length=64)),
                ('start_date', models.DateTimeField(default=datetime.date(2020, 9, 1), null=True)), # noqa
            ],
        ),
    ]
