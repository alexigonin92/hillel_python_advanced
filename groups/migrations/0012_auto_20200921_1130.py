# Generated by Django 3.1 on 2020-09-21 11:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('teachers', '0008_remove_teacher_age'),
        ('students', '0013_student_group'),
        ('groups', '0011_group_teacher'),
    ]

    operations = [
        migrations.AddField(
            model_name='group',
            name='curator',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='curator', to='teachers.teacher'),
        ),
        migrations.AddField(
            model_name='group',
            name='leader',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='leader', to='students.student'),
        ),
    ]
