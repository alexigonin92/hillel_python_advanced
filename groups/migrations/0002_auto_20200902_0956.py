import datetime

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('groups', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='start_date',
            field=models.DateTimeField(default=datetime.date(2020, 9, 2),
                                       null=True),
        ),
    ]
