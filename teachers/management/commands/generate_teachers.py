from django.core.management.base import BaseCommand

from faker import Faker

from teachers.models import Teacher


class Command(BaseCommand):
    help = 'Creates teacher objects' # noqa

    def add_arguments(self, parser):
        parser.add_argument('total', type=int,
                            help=u'Количество создаваемых пользователей')

    def handle(self, *args, **kwargs):
        faker = Faker()
        total = kwargs['total']

        for _ in range(total):
            Teacher.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
            )
