from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from teachers.models import Teacher

from .forms import TeacherCreateForm


class TeacherListView(LoginRequiredMixin, ListView):
    model = Teacher
    template_name = 'teacher-get.html'
    context_object_name = 'teachers'
    paginate_by = 6

    def get_queryset(self):
        """
        Функция осуществляет фильтрацию

        super().get_queryset() - наследование от базового класса. Аналог записи Teacher.objects.all()

        self.request - в классе TeacherListView есть доступ к request

        qr - список обьектов класса Teacher.
        """
        qr = super().get_queryset()
        request = self.request

        first_name = request.GET.get('first_name')
        last_name = request.GET.get('last_name')

        if first_name:
            qr = qr.filter(first_name=first_name)
        if last_name:
            qr = qr.filter(last_name=last_name)

        return qr


class TeacherCreateView(LoginRequiredMixin, CreateView):
    """
    Класс реализует функционал создания учителя

     UpdateView - встроенный класс Джанго, стандарная реализация создания.
        переменные задаются как написано в документации.
    """
    model = Teacher
    form_class = TeacherCreateForm
    success_url = reverse_lazy('teachers:list')
    template_name = 'teacher-create.html'


class TeahcerUpdateView(UpdateView):
    """
         Класс реализует функционал редактирования учителей

         UpdateView - встроенный класс Джанго, стандарная реализация редактирования.
            переменные задаются как написано в документации.

         # get_object - возвращает учителя по uu_id.

         get_context_data - расширяет словарь context в функции render()

        """
    model = Teacher
    form_class = TeacherCreateForm
    success_url = reverse_lazy('teachers:list')
    template_name = 'teacher-edit.html'
    context_object_name = 'teacher'

    def get_object(self):
        """
        self - это обьект класса TeacherUpdateView

        self.kwargs.get('uuid') - kwargs это словарь, get('uuid') функция возвращает значение ключа 'uuid'

        self.get_queryset() - возвращает объект модели. В данном случае по конкретному айдишнику.
        """

        uuid = self.kwargs.get('uuid')
        return self.get_queryset().get(uu_id=uuid)

    def get_context_data(self, **kwargs):
        """
        ????  super().get_context_data(**kwargs) -- Не совсем понял как это работает ????
        """
        context = super().get_context_data(**kwargs)
        context['groups'] = self.get_object().groups.all()
        return context


class TeacherdeleteView(LoginRequiredMixin, DeleteView):
    model = Teacher
    success_url = reverse_lazy('teachers:list')

    def get_object(self):
        """
        self - это обьект класса TeacherDeleteView

        self.kwargs.get('uuid') - kwargs это словарь, get('uuid') функция возвращает значение ключа 'uuid'

        self.get_queryset() - возвращает объект модели. В данном случае по конкретному айдишнику.
        """

        uuid = self.kwargs.get('uuid')
        return self.get_queryset().get(uu_id=uuid)

    def delete(self, request, *args, **kwargs):
        """
        Стандартный метод базового класса

        self.get_object() - Принимает один обьект из метода get_object, который я переопределил

        self.get_success_url() - подхватывает атрибут класса
        """
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.delete()
        return HttpResponseRedirect(success_url)
