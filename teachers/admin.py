from django.contrib import admin # noqa

# Register your models here.
from groups.models import Group

from teachers.models import Teacher


class Groups(admin.TabularInline):
    model = Group
    fk_name = 'teacher'
    fields = ['name']
    readonly_fields = fields
    show_change_link = True


class TeacherAdmin(admin.ModelAdmin):
    exclude = ['uu_id']
    inlines = [Groups]


admin.site.register(Teacher, TeacherAdmin)
