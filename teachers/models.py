import uuid

from core.models import Person

from faker import Faker


class Teacher(Person):

    @staticmethod
    def create_teacher(teacher_count):
        fake = Faker()

        for _ in range(int(teacher_count)):
            f_name = fake.first_name()
            l_name = fake.last_name()
            uu_id = uuid.uuid4()

            Teacher.objects.create(first_name=f_name, last_name=l_name,
                                   uu_id=uu_id)
