# Generated by Django 3.1 on 2020-09-07 14:34

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('teachers', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='teacher',
            name='language',
        ),
    ]
