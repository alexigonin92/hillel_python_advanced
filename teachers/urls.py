from django.urls import path

from teachers.views import TeacherCreateView, TeacherListView, TeacherdeleteView, TeahcerUpdateView

app_name = 'teachers'

urlpatterns = [
    path('', TeacherListView.as_view(), name='list'),
    path('create/', TeacherCreateView.as_view(), name='create'),
    path('edit/<uuid:uuid>', TeahcerUpdateView.as_view(), name='edit'),
    path('delete/<uuid:uuid>', TeacherdeleteView.as_view(), name='delete'),
]
