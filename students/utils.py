import random


def gen_password(length):
    result = ''.join([
        str(random.randint(0, 9))
        for _ in range(length)
    ])
    return result


def parse_lenght(request, default=10):
    length = request.GET.get('length', str(default))

    if not length.isnumeric():
        raise ValueError("VALUE ERROR: int")

    length = int(length)

    if not 3 < length < 100:
        raise ValueError("RANGE ERROR: [3..10]")

    return length

# -------------------------------Homework 6----------------------------


def check_value(customer_count):

    if customer_count is None:
        raise ValueError('Enter customer count')

    if not customer_count.isnumeric():
        raise ValueError('Count must be positive interger number')

    customer_count = int(customer_count)

    if not 0 < customer_count <= 100:
        # синтаксис этого условия как-то сложно понять ))
        raise ValueError('Customers count must be > 0 and < 101')


def format_list(lst):
    return '<br>'.join(str(elem) for elem in lst)
