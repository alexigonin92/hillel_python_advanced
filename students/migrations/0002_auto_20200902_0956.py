import datetime

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='birthdate',
            field=models.DateTimeField(default=datetime.date(2020, 9, 2),
                                       null=True),
        ),
    ]
