import random
import uuid

from core.models import Person

from django.db import models

from faker import Faker

from groups.models import Group


class Student(Person):

    rating = models.SmallIntegerField(null=True, default=0)
    phone_number = models.CharField(max_length=64, null=True, blank=True)
    group = models.ForeignKey(to=Group, null=True, on_delete=models.SET_NULL, related_name='students')

    def __str__(self):
        return f'{super().__str__()}, {self.rating}'

    @staticmethod
    def create_student(student_count):
        fake = Faker()
        groups = list(Group.objects.all())

        for _ in range(int(student_count)):
            f_name = fake.first_name()
            l_name = fake.last_name()
            rating = random.randint(0, 100)
            uu_id = uuid.uuid4()
            group = random.choice(groups)

            Student.objects.create(first_name=f_name, last_name=l_name,
                                   rating=rating, uu_id=uu_id, group=group)
