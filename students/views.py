from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from students.forms import StudentsCreateForm
from students.models import Student
from students.utils import gen_password, parse_lenght


def hello(request):
    return HttpResponse('Hello from Django!')


def get_random(request):
    try:
        length = parse_lenght(request, 10)
    except Exception as ex:
        return HttpResponse(str(ex), status_code=400)

    result = gen_password(length)

    return HttpResponse(result)


class StudentListView(LoginRequiredMixin, ListView):
    model = Student
    template_name = 'student-get.html'
    context_object_name = 'students'
    paginate_by = 6

    def get_queryset(self):
        """
        Функция осуществляет фильтрацию

        super().get_queryset() - наследование от базового класса

        self.request - в классе StudentListView есть доступ к request

        qr - список обьектов класса Student. Аналог записи Student.objects.all()
        """
        qr = super().get_queryset()
        request = self.request

        first_name = request.GET.get('first_name')
        last_name = request.GET.get('last_name')
        rating = request.GET.get('rating')

        if first_name:
            qr = qr.filter(first_name=first_name)
        if last_name:
            qr = qr.filter(last_name=last_name)
        if rating:
            qr = qr.filter(rating=rating)

        return qr


class StudentCreateView(LoginRequiredMixin, CreateView):
    """
    Класс реализует функционал создания студента

     UpdateView - встроенный класс Джанго, стандарная реализация создания.
        переменные задаются как написано в документации.
    """
    model = Student
    form_class = StudentsCreateForm
    success_url = reverse_lazy('students:list')
    template_name = 'student-create.html'


class StudentUpdateView(LoginRequiredMixin, UpdateView):
    """
     Класс реализует функционал редактирования студента

     UpdateView - встроенный класс Джанго, стандарная реализация редактирования.
        переменные задаются как написано в документации.

     # get_object - возвращает студента по uu_id.
    """
    model = Student
    form_class = StudentsCreateForm
    success_url = reverse_lazy('students:list')
    template_name = 'student-edit.html'
    context_object_name = 'student'

    def get_object(self):
        """
        self - это обьект класса StudentUpdateView

        self.kwargs.get('uuid') - kwargs это словарь, get('uuid') функция возвращает значение ключа 'uuid'

        self.get_queryset() - возвращает объект модели. В данном случае по конкретному айдишнику.
        """

        uuid = self.kwargs.get('uuid')
        return self.get_queryset().get(uu_id=uuid)


class StudentDeleteView(LoginRequiredMixin, DeleteView):
    model = Student
    success_url = reverse_lazy('students:list')

    def get_object(self):
        """
        self - это обьект класса StudentDeleteView

        self.kwargs.get('uuid') - kwargs это словарь, get('uuid') функция возвращает значение ключа 'uuid'

        self.get_queryset() - возвращает объект модели. В данном случае по конкретному айдишнику.
        """

        uuid = self.kwargs.get('uuid')
        return self.get_queryset().get(uu_id=uuid)

    def delete(self, request, *args, **kwargs):
        """
        Стандартный метод базового класса

        self.get_object() - Принимает один обьект из метода get_object, который я переопределил

        self.get_success_url() - подхватывает атрибут класса
        """
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.delete()
        return HttpResponseRedirect(success_url)
