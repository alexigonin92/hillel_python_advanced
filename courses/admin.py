from courses.models import Course, Technology

from django.contrib import admin


admin.site.register(Technology)
admin.site.register(Course)
