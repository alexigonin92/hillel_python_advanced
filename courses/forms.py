import Course

from django import forms


class StudentsCreateForm(forms.ModelForm):
    class Meta():
        model = Course
        fields = '__all__'
