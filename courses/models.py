from django.db import models


class Technology(models.Model):
    name = models.CharField(max_length=64, null=True)

    def __str__(self):
        return self.name


class Course(models.Model):
    name = models.CharField(max_length=64)

    technologies = models.ManyToManyField(
        to=Technology,
        related_name='courses'
    )

    def __str__(self):
        return self.name
