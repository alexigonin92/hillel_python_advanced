from accounts.views import AccountCreateView, AccountLoginView, AccountLogoutView, AccountUpdateView, PasswordUpdateView

from django.urls import path


app_name = 'accounts'

urlpatterns = [
    path('register/', AccountCreateView.as_view(), name='register'),
    path('login/', AccountLoginView.as_view(), name='login'),
    path('logout/', AccountLogoutView.as_view(), name='logout'),
    path('profile/', AccountUpdateView.as_view(), name='profile'),
    path('password/', PasswordUpdateView.as_view(), name='password'),

]
