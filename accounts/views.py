import os

from PIL import Image

from accounts.forms import AccountCreateForm, AccountProfileUpdateForm, AccountUpdateForm
from accounts.models import Profile, UserActions

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView
from django.views.generic.edit import ProcessFormView


class AccountCreateView(CreateView):
    model = User
    template_name = 'registration.html'
    form_class = AccountCreateForm
    success_url = reverse_lazy('accounts:login')

    def form_valid(self, form):

        messages.info(self.request, 'Successfully registration')

        return super().form_valid(form)


class AccountLoginView(LoginView):
    model = UserActions
    template_name = 'login.html'
    action = model.USER_ACTION.LOGIN

    def get_redirect_url(self):
        if self.request.GET.get('next'):
            return self.request.GET.get('next')
        return reverse('core:index')

    def form_valid(self, form):
        result = super().form_valid(form)

        self.model.user_action(self.request.POST.get('username'), self.action)

        try:
            profile = self.request.user.profile
        except Exception:
            profile = Profile.objects.create(user=self.request.user)
            profile.save()

        messages.info(self.request, f'User {self.request.POST.get("username")} has been successfully logged in')

        return result


class AccountLogoutView(LogoutView):
    template_name = 'logout.html'

    def dispatch(self, request, *args, **kwargs):

        messages.info(self.request, 'Successfully logout')

        return super().dispatch(request, *args, **kwargs)


class PasswordUpdateView(PasswordChangeView):
    template_name = 'password_change_form.html'
    success_url = reverse_lazy('accounts:profile')

    def form_valid(self, form):

        messages.info(self.request, 'Successfully password update')

        return super().form_valid(form)


class AccountUpdateView(LoginRequiredMixin, ProcessFormView):
    """Создаю свою вью функцию, которая отображает две формы сразу"""
    def get(self, request, *args, **kwargs):
        """Сюда приходит гет запрос, обрабатывается и отображается на страницу"""
        user = self.request.user
        profile = self.request.user.profile

        user_form = AccountUpdateForm(instance=user)
        profile_form = AccountProfileUpdateForm(instance=profile)

        messages.info(self.request, 'Successfully account update')

        return render(
            request=request,
            template_name='profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form
            }
        )

    def post(self, request, *args, **kwargs):
        """Сюда приходит пост запрос от клиента, и новые данные сохраня.тся в модель"""
        user = self.request.user
        profile = self.request.user.profile
        action = UserActions
        current_image_name = profile.image.name

        user_form = AccountUpdateForm(
            data=request.POST,
            instance=user)
        profile_form = AccountProfileUpdateForm(
            data=request.POST,
            files=request.FILES,
            instance=profile)

        if user_form.is_valid() and profile_form.is_valid():

            if self.request.FILES:
                action.user_action(
                    user_name=user.username,
                    user_action=action.USER_ACTION.CHANGE_PROFILE_IMAGE,
                    user_info=f'{current_image_name} -> {profile.image.name}'
                )

            user_form.save()
            profile_form.save()

            if self.request.FILES and int(profile.image.width) > profile.MAX_IMAGE_WIDTH:

                path = os.path.join(settings.MEDIA_ROOT, profile.image.name)

                im = Image.open(path)
                im.thumbnail((profile.MAX_IMAGE_WIDTH, profile.MAX_IMAGE_HEIGHT))
                im.save(path)

            return HttpResponseRedirect(reverse('accounts:profile'))

        return render(
            request=request,
            template_name='profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form
            }
        )
