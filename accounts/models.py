from django.contrib.auth.models import User
from django.db import models


class UserActions(models.Model):
    class USER_ACTION(models.IntegerChoices):
        LOGIN = 0, "Login"
        LOGOUT = 1, "Logout"
        CHANGE_PASSWORD = 2, "Change Password"
        CHANGE_PROFILE = 3, "Change Profile"
        CHANGE_PROFILE_IMAGE = 4, "Change Profile Image"

    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    write_date = models.DateTimeField(auto_now_add=True)
    action = models.PositiveSmallIntegerField(choices=USER_ACTION.choices)
    info = models.CharField(max_length=128, null=True)

    @classmethod
    def user_action(cls, user_name, user_action, user_info=None):
        user = User.objects.get(username=user_name)
        action = user_action
        info = user_info

        UserActions.objects.create(user=user, action=action, info=info)


class Profile(models.Model):
    user = models.OneToOneField(
        to=User,  # settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="profile"
    )
    image = models.ImageField(null=True, default='default.jpg', upload_to='pics/')
    interests = models.CharField(max_length=128, null=True)
    MAX_IMAGE_WIDTH = 300
    MAX_IMAGE_HEIGHT = 200
