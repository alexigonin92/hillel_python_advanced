from accounts.models import Profile

from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm


class AccountCreateForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'password1', 'password2']


class AccountUpdateForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        fields = ['username']


class AccountProfileUpdateForm(ModelForm):
    class Meta():
        model = Profile
        fields = ['image', 'interests']
