import copy
import time


class Shape:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y


obj = Shape(5, 10)


class Point(Shape):

    def __init__(self, x, y):
        super().__init__(x, y)


class Circle(Shape):
    def __init__(self, x, y, radius):
        super().__init__(x, y)
        self.radius = radius

    def __contains__(self, item):
        if type(item) == Point:
            return Circle.point_in_circle(self, item)
        else:
            raise ValueError('This is not Point object')

    def point_in_circle(self, point):

        if (point.x - self.x)**2 + (point.y - self.y)**2 < self.radius**2:
            return True
        return False


circle = Circle(4, 6, 15)
point = Point(2, 3)

print(point in circle)
print(circle.point_in_circle(point))


class lazy_object:

    def __init__(self, callable, *args, **kw): # noqa
        '''
        callable -- Class of objeсt to be instantiated or functionnn to be called
        *args -- arguments to be used when instantiating object
        **kw  -- keywords to be used when instantiating object
        '''
        self.__dict__['callable'] = callable
        self.__dict__['args'] = args
        self.__dict__['kw'] = kw
        self.__dict__['obj'] = None

    def init_obj(self):
        '''
        Instantiate object if not already done
        '''
        if self.obj is None:
            self.__dict__['obj'] = self.callable(*self.args, **self.kw)

    def __getattr__(self, name):
        self.init_obj()
        return getattr(self.obj, name)

    def __setattr__(self, name, value):
        self.init_obj()
        setattr(self.obj, name, value)

    def __len__(self):
        self.init_obj()
        return len(self.obj)

    def __getitem__(self, idx):
        self.init_obj()
        return self.obj[idx]

    def __copy__(self):
        new_copy = lazy_object(self.callable, self.args, self.kw)
        new_copy.__dict__['obj'] = copy.copy(self.obj)
        return new_copy

    def __deepcopy__(self):
        new_copy = lazy_object(self.callable, self.args, self.kw)
        new_copy.__dict__['obj'] = copy.deepcopy(self.obj)
        return new_copy


class timer():
    def __init__(self, message):
        self.message = message

    def __enter__(self):
        self.start = time.time()
        return None

    def __exit__(self, type, value, traceback): # noqa
        elapsed_time = (time.time() - self.start) * 1000
        print(self.message.format(elapsed_time))


l_obj1 = lazy_object(Shape, 2, 2)
l_obj2 = lazy_object(Shape, 2, 2)

with timer('time spend {}'):
    l_obj3 = l_obj1.__copy__()

with timer('time spend {}'):
    l_obj4 = l_obj2.__deepcopy__()
